
<h1 align="center">
  <br>
  <a href="https://github.com/KartSriv/ICSE-Project-for-Computer/"><img src="https://cdn.freebiesupply.com/logos/large/2x/java-14-logo-png-transparent.png" alt="Markdownify" width="200"></a>
</h1>

<h4 align="center">I know the pain of programming the whole code by scratch. So, here is your solution: <b>Last year's Completely Original, Swing-based GUI (Not AWT) program, the program whhich no teacher can question about and Works in most IDEs.</b> It's free and Open-Source so you can use it with no fear of getting sued. To download all files <a href="https://codeload.github.com/KartSriv/Arduino-Snippets/zip/master" target="_blank">Click Here</a>.</h4>
  </a>

 <P>If you want to say thanks then click this button    <a href="https://saythanks.io/to/KartSriv"><img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg">
  </a>
</p>

**Tested on Intelij(Jetbrains)** - 2/152019 <BR>
**Tested on Eclipse** - 2/15/2019



[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://github.com/KartSriv/)
[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://github.com/KartSriv/)
<br>
LICENSE: GNU General Public License v3.0
<br>

![ForTheBadge built-with-love](https://i.imgur.com/PSF4H8o.png)

