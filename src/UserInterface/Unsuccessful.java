package UserInterface;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;

public class Unsuccessful {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Unsuccessful window = new Unsuccessful();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Unsuccessful() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(235,84,99));
		frame.getContentPane().setForeground(Color.BLACK);
		frame.getContentPane().setLayout(null);
		
		JLabel lblBookingUnsuccessful = new JLabel("Booking Unsuccessful");
		lblBookingUnsuccessful.setForeground(Color.WHITE);
		lblBookingUnsuccessful.setFont(new Font("Product Sans", Font.PLAIN, 40));
		lblBookingUnsuccessful.setBackground(Color.WHITE);
		lblBookingUnsuccessful.setBounds(28, 54, 462, 50);
		frame.getContentPane().add(lblBookingUnsuccessful);
		
		JLabel label_1 = new JLabel("* Don't forget to get your passport.");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("Product Sans", Font.PLAIN, 20));
		label_1.setBackground(Color.WHITE);
		label_1.setBounds(28, 144, 357, 50);
		frame.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("* Always ask for your Boarding Pass.");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Product Sans", Font.PLAIN, 20));
		label_2.setBackground(Color.WHITE);
		label_2.setBounds(28, 205, 357, 50);
		frame.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("* Check your baggage weight before travelling to the Airport");
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("Product Sans", Font.PLAIN, 20));
		label_3.setBackground(Color.WHITE);
		label_3.setBounds(28, 266, 574, 50);
		frame.getContentPane().add(label_3);
		
		JLabel lblIncaseYoureTravelling = new JLabel("Incase you're travelling");
		lblIncaseYoureTravelling.setForeground(Color.WHITE);
		lblIncaseYoureTravelling.setFont(new Font("Product Sans", Font.PLAIN, 20));
		lblIncaseYoureTravelling.setBackground(Color.WHITE);
		lblIncaseYoureTravelling.setBounds(28, 102, 357, 50);
		frame.getContentPane().add(lblIncaseYoureTravelling);
		frame.setBounds(100, 100, 906, 504);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
