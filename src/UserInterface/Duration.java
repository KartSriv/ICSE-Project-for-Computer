package UserInterface;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTree;
import javax.swing.JSeparator;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;

public class Duration {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Duration window = new Duration();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Duration() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(52,152,219));
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Duration.class.getResource("/Images/AirFrance.jpg")));
		label.setBounds(644, 0, 486, 518);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(601, 0, 486, 518);
		frame.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("Airline Reservation System");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Product Sans", Font.PLAIN, 30));
		label_2.setBackground(Color.WHITE);
		label_2.setBounds(23, 26, 357, 37);
		frame.getContentPane().add(label_2);
		
		textField = new JTextField();
		textField.setBounds(23, 244, 197, 37);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		frame.getContentPane().add(textField);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(242, 244, 197, 37);
		frame.getContentPane().add(textField_1);
		frame.getContentPane().add(textField_1);
		
		JLabel lblFrom = new JLabel("From");
		lblFrom.setForeground(Color.WHITE);
		lblFrom.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		lblFrom.setBackground(Color.WHITE);
		lblFrom.setBounds(23, 116, 124, 37);
		frame.getContentPane().add(lblFrom);
		
		JLabel lblTo = new JLabel("To");
		lblTo.setForeground(Color.WHITE);
		lblTo.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		lblTo.setBackground(Color.WHITE);
		lblTo.setBounds(242, 116, 124, 37);
		frame.getContentPane().add(lblTo);
		
		String[] fromStrings = { "India", "USA", "Russia", "Europe (Any Country)", "Australia" , "Asia (Any Country)" };
		String[] toStrings = { "USA", "India", "Russia", "Europe (Any Country)", "Australia" , "Asia (Any Country)" };
		
		JComboBox comboBox = new JComboBox(fromStrings);
		comboBox.setBounds(23, 152, 197, 28);
		frame.getContentPane().add(comboBox);
		String selectedcode = (String) comboBox.getSelectedItem();
		//frame.getContentPane().combobox.getItemAt(String L1)
		
		JComboBox comboBox_1 = new JComboBox(toStrings);
		comboBox_1.setBounds(242, 149, 197, 28);
		frame.getContentPane().add(comboBox_1);
		String selectedcod1 = (String)comboBox_1.getSelectedItem();
		//frame.getContentPane().combobox_1.getItemAt(String L2)

		
		JLabel lblLocation = new JLabel("Location");
		lblLocation.setForeground(Color.WHITE);
		lblLocation.setFont(new Font("Product Sans", Font.PLAIN, 20));
		lblLocation.setBackground(Color.WHITE);
		lblLocation.setBounds(23, 90, 357, 37);
		frame.getContentPane().add(lblLocation);
		
		JLabel lblDatemmddyyyy = new JLabel("Date (MM/DD/YYYY)");
		lblDatemmddyyyy.setForeground(Color.WHITE);
		lblDatemmddyyyy.setFont(new Font("Product Sans", Font.PLAIN, 20));
		lblDatemmddyyyy.setBackground(Color.WHITE);
		lblDatemmddyyyy.setBounds(23, 191, 357, 37);
		frame.getContentPane().add(lblDatemmddyyyy);
		
		JLabel label_4 = new JLabel("From");
		label_4.setForeground(Color.WHITE);
		label_4.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		label_4.setBackground(Color.WHITE);
		label_4.setBounds(23, 213, 124, 37);
		frame.getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("To");
		label_5.setForeground(Color.WHITE);
		label_5.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		label_5.setBackground(Color.WHITE);
		label_5.setBounds(242, 213, 124, 37);
		frame.getContentPane().add(label_5);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("By using the Airline Reservation System, you agree to the\r\n Privacy and Transparency Agreement.");
		chckbxNewCheckBox.setForeground(Color.WHITE);
		chckbxNewCheckBox.setBackground(Color.RED);
		chckbxNewCheckBox.setFont(new Font("Product Sans", Font.PLAIN, 13));
		chckbxNewCheckBox.setBounds(23, 315, 572, 28);
		frame.getContentPane().add(chckbxNewCheckBox);
		
		JCheckBox chckbxDataOnApplication = new JCheckBox("Data on Application is only for illustration purposes only.");
		chckbxDataOnApplication.setForeground(Color.BLACK);
		chckbxDataOnApplication.setFont(new Font("Product Sans", Font.PLAIN, 13));
		chckbxDataOnApplication.setBackground(Color.YELLOW);
		chckbxDataOnApplication.setBounds(23, 346, 345, 28);
		frame.getContentPane().add(chckbxDataOnApplication);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String text = textField.getText();
				System.out.println(text);
				String text1 = textField_1.getText();
				System.out.println(text1);
				try {
					PrintWriter writer = new PrintWriter("DurationLog.txt", "UTF-8");
					writer.println(text);
					writer.println(text1);
					//writer.println(Babies);
					writer.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				//lectedcode
				try {
					PrintWriter writer = new PrintWriter("LocationLog.txt", "UTF-8");
					writer.println(selectedcode);
					writer.println(selectedcod1);
					//writer.println(Babies);
					writer.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
                FinalBooking.main(null); 

			}
		});
		btnSubmit.setBounds(499, 483, 117, 23);
		frame.getContentPane().add(btnSubmit);
		frame.setBounds(100, 100, 1093, 556);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
}
