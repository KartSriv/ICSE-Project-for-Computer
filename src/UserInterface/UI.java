package UserInterface;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.awt.EventQueue;
import java.io.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;


public class UI {

	public JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UI window = new UI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(52,152,219));
		frame.setBounds(100, 100, 1093, 554);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblAirlineReservationSystem = new JLabel("Airline Reservation System");
		lblAirlineReservationSystem.setForeground(Color.WHITE);
		lblAirlineReservationSystem.setBackground(Color.WHITE);
		lblAirlineReservationSystem.setFont(new Font("Product Sans", Font.PLAIN, 30));
		lblAirlineReservationSystem.setBounds(23, 25, 357, 37);
		frame.getContentPane().add(lblAirlineReservationSystem);
		
		textField = new JTextField();
		textField.setBackground(Color.WHITE);
		textField.setFont(new Font("Product Sans", Font.PLAIN, 20));
		textField.setBounds(23, 130, 417, 37);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNumberOfAdults = new JLabel("Number of Adults");
		lblNumberOfAdults.setForeground(Color.WHITE);
		lblNumberOfAdults.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		lblNumberOfAdults.setBackground(Color.WHITE);
		lblNumberOfAdults.setBounds(23, 82, 124, 37);
		frame.getContentPane().add(lblNumberOfAdults);
		
		JLabel lblNumberOfChildren = new JLabel("Number of Children (If any)");
		lblNumberOfChildren.setForeground(Color.WHITE);
		lblNumberOfChildren.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		lblNumberOfChildren.setBackground(Color.WHITE);
		lblNumberOfChildren.setBounds(23, 186, 188, 37);
		frame.getContentPane().add(lblNumberOfChildren);
		
		textField_1 = new JTextField();
		textField_1.setBackground(Color.WHITE);
		textField_1.setFont(new Font("Product Sans", Font.PLAIN, 20));
		textField_1.setColumns(10);
		textField_1.setBounds(23, 234, 417, 37);
		frame.getContentPane().add(textField_1);
		
		JLabel lblNumberOfBabies = new JLabel("Number of Babies (If any) **");
		lblNumberOfBabies.setForeground(Color.WHITE);
		lblNumberOfBabies.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		lblNumberOfBabies.setBackground(Color.WHITE);
		lblNumberOfBabies.setBounds(23, 282, 200, 37);
		frame.getContentPane().add(lblNumberOfBabies);
		
		textField_2 = new JTextField();
		textField_2.setBackground(Color.WHITE);
		textField_2.setFont(new Font("Product Sans", Font.PLAIN, 20));
		textField_2.setColumns(10);
		textField_2.setBounds(23, 330, 417, 37);
		frame.getContentPane().add(textField_2);
		
		JLabel lblBabySeats = new JLabel("** Baby Seats are Subject to Availabilty.");
		lblBabySeats.setForeground(Color.WHITE);
		lblBabySeats.setFont(new Font("Segoe UI", Font.PLAIN, 10));
		lblBabySeats.setBackground(Color.WHITE);
		lblBabySeats.setBounds(23, 360, 284, 37);
		frame.getContentPane().add(lblBabySeats);
		
		JLabel lblBabySeats_1 = new JLabel("** Baby Seats varies Airline to Airline.");
		lblBabySeats_1.setForeground(Color.WHITE);
		lblBabySeats_1.setFont(new Font("Segoe UI", Font.PLAIN, 10));
		lblBabySeats_1.setBackground(Color.WHITE);
		lblBabySeats_1.setBounds(23, 378, 284, 37);
		frame.getContentPane().add(lblBabySeats_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(UI.class.getResource("/Images/Emirates.jpg")));
		lblNewLabel.setBounds(591, 0, 486, 518);
		frame.getContentPane().add(lblNewLabel);
		
		//String nextProg = "Airline.java";
		//String nextProg = "Airline.java";
		
		JButton btnCheckForAvailabilty = new JButton("Check for Availabilty");
		btnCheckForAvailabilty.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Adults = textField.getText();
				System.out.println(Adults);
				String Children = textField_1.getText();
				System.out.println(Children);
				String Babies = textField_2.getText();
				System.out.println(Babies);
				int sum = Integer.parseInt(Adults);	
				sum += Integer.parseInt(Children);
				sum += Integer.parseInt(Babies);
				if (sum >= 2)
				JOptionPane.showMessageDialog(frame, "You have allocated tickets for "+sum+" people. Do you want to continue?");
				else
				JOptionPane.showMessageDialog(frame, "You have allocated tickets for only 1 person. Do you want to continue?");
                //System.exit(0);
                //Airline tf2 =new Airline();
                //tf2.setVisible(true);
				//dispose();
				try {
					PrintWriter writer = new PrintWriter("PassangerLog.txt", "UTF-8");
					writer.println(Adults);
					writer.println(Children);
					writer.println(Babies);
					writer.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				//System.exit(0);
                Airline.main(null); 
			}
	});
		btnCheckForAvailabilty.setBounds(416, 473, 165, 23);
		frame.getContentPane().add(btnCheckForAvailabilty);	
}
}