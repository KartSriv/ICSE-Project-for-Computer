package UserInterface;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;

public class EnjoyTrip {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EnjoyTrip window = new EnjoyTrip();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EnjoyTrip() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(52,152,219));
		frame.getContentPane().setLayout(null);
		
		JLabel lblHaveASafe = new JLabel("Have a Safe Trip!");
		lblHaveASafe.setForeground(Color.WHITE);
		lblHaveASafe.setFont(new Font("Product Sans", Font.PLAIN, 40));
		lblHaveASafe.setBackground(Color.WHITE);
		lblHaveASafe.setBounds(28, 58, 357, 50);
		frame.getContentPane().add(lblHaveASafe);
		
		JLabel lblDontForget = new JLabel("* Don't forget to get your passport.");
		lblDontForget.setForeground(Color.WHITE);
		lblDontForget.setFont(new Font("Product Sans", Font.PLAIN, 20));
		lblDontForget.setBackground(Color.WHITE);
		lblDontForget.setBounds(28, 148, 357, 50);
		frame.getContentPane().add(lblDontForget);
		
		JLabel lblAlwaysAsk = new JLabel("* Always ask for your Boarding Pass.");
		lblAlwaysAsk.setForeground(Color.WHITE);
		lblAlwaysAsk.setFont(new Font("Product Sans", Font.PLAIN, 20));
		lblAlwaysAsk.setBackground(Color.WHITE);
		lblAlwaysAsk.setBounds(28, 209, 357, 50);
		frame.getContentPane().add(lblAlwaysAsk);
		
		JLabel lblCheckYour = new JLabel("* Check your baggage weight before travelling to the Airport");
		lblCheckYour.setForeground(Color.WHITE);
		lblCheckYour.setFont(new Font("Product Sans", Font.PLAIN, 20));
		lblCheckYour.setBackground(Color.WHITE);
		lblCheckYour.setBounds(28, 270, 574, 50);
		frame.getContentPane().add(lblCheckYour);
		frame.setBounds(100, 100, 906, 504);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
