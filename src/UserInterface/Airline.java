package UserInterface;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JCheckBox;
import javax.swing.JButton;

public class Airline {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Airline window = new Airline();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Airline() {
		initialize();
	}

	int TrueCounter = 0;

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(52,152,219));
		frame.setBounds(100, 100, 1093, 556);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Airline.class.getResource("/Images/LufthansaBackPicture.jpg")));
		label.setBounds(591, 0, 486, 518);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("Airline Reservation System");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("Product Sans", Font.PLAIN, 30));
		label_1.setBackground(Color.WHITE);
		label_1.setBounds(23, 26, 357, 37);
		frame.getContentPane().add(label_1);
		
		JCheckBox a = new JCheckBox("Lufthansa");
		a.setForeground(Color.WHITE);
		a.setBackground(new Color(52,152,219));
		a.setFont(new Font("Product Sans", Font.PLAIN, 15));
		a.setBounds(34, 117, 128, 23);
		frame.getContentPane().add(a);
		
		JCheckBox b = new JCheckBox("Emirates");
		b.setForeground(Color.WHITE);
		b.setBackground(new Color(52,152,219));
		b.setFont(new Font("Product Sans", Font.PLAIN, 15));
		b.setBounds(34, 155, 128, 23);
		frame.getContentPane().add(b);
		
		JCheckBox c = new JCheckBox("Etihad");
		c.setForeground(Color.WHITE);
		c.setBackground(new Color(52,152,219));
		c.setFont(new Font("Product Sans", Font.PLAIN, 15));
		c.setBounds(34, 194, 128, 23);
		frame.getContentPane().add(c);
		
		JCheckBox d = new JCheckBox("British Airways");
		d.setForeground(Color.WHITE);
		d.setBackground(new Color(52,152,219));
		d.setFont(new Font("Product Sans", Font.PLAIN, 15));
		d.setBounds(34, 235, 128, 23);
		frame.getContentPane().add(d);
		
		JCheckBox m = new JCheckBox("Air India");
		m.setForeground(Color.WHITE);
		m.setBackground(new Color(52,152,219));
		m.setFont(new Font("Product Sans", Font.PLAIN, 15));
		m.setBounds(188, 119, 128, 23);
		frame.getContentPane().add(m);
		
		JCheckBox f = new JCheckBox("Air France");
		f.setForeground(Color.WHITE);
		f.setBackground(new Color(52,152,219));
		f.setFont(new Font("Product Sans", Font.PLAIN, 15));
		f.setBounds(188, 157, 128, 23);
		frame.getContentPane().add(f);
		
		JCheckBox g = new JCheckBox("Malayasia Airlines");
		g.setForeground(Color.WHITE);
		g.setBackground(new Color(52,152,219));
		g.setFont(new Font("Product Sans", Font.PLAIN, 15));
		g.setBounds(188, 196, 153, 23);
		frame.getContentPane().add(g);
		
		JCheckBox h = new JCheckBox("Jet Airways");
		h.setForeground(Color.WHITE);
		h.setBackground(new Color(52,152,219));
		h.setFont(new Font("Product Sans", Font.PLAIN, 15));
		h.setBounds(188, 237, 128, 23);
		frame.getContentPane().add(h);
		
		JCheckBox i = new JCheckBox("KLM (Dutch)");
		i.setForeground(Color.WHITE);
		i.setBackground(new Color(52,152,219));
		i.setFont(new Font("Product Sans", Font.PLAIN, 15));
		i.setBounds(343, 237, 128, 23);
		frame.getContentPane().add(i);
		
		JCheckBox j = new JCheckBox("Swiss Air");
		j.setForeground(Color.WHITE);
		j.setBackground(new Color(52,152,219));
		j.setFont(new Font("Product Sans", Font.PLAIN, 15));
		j.setBounds(343, 196, 128, 23);
		frame.getContentPane().add(j);
		
		JCheckBox k = new JCheckBox("Singapore Airlines");
		k.setForeground(Color.WHITE);
		k.setBackground(new Color(52,152,219));
		k.setFont(new Font("Product Sans", Font.PLAIN, 15));
		k.setBounds(343, 157, 153, 23);
		frame.getContentPane().add(k);
		
		JCheckBox l = new JCheckBox("Gulf Air");
		l.setForeground(Color.WHITE);
		l.setBackground(new Color(52,152,219));
		l.setFont(new Font("Product Sans", Font.PLAIN, 15));
		l.setBounds(343, 119, 128, 23);
		frame.getContentPane().add(l);
				
		JButton button = new JButton("Check for Availabilty");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean aair = a.isSelected();
				System.out.println(aair);
				if (aair == true)
					TrueCounter++;
				boolean bair = b.isSelected();
				System.out.println(bair);
				if (bair == true)
					TrueCounter++;
				boolean cair = c.isSelected();
				System.out.println(cair);
				if (cair == true)
					TrueCounter++;
				boolean dair = d.isSelected();
				System.out.println(dair);
				if (dair == true)
					TrueCounter++;
				boolean mair = m.isSelected();
				System.out.println(mair);
				if (mair == true)
					TrueCounter++;
				boolean fair = f.isSelected();
				System.out.println(fair);
				if (fair == true)
					TrueCounter++;
				boolean gair = g.isSelected();
				System.out.println(gair);
				if (gair == true)
					TrueCounter++;
				boolean hair = h.isSelected();
				System.out.println(hair);
				if (hair == true)
					TrueCounter++;
				boolean iair = i.isSelected();
				System.out.println(iair);
				if (iair == true)
					TrueCounter++;
				boolean jair = j.isSelected();
				System.out.println(jair);
				if (jair == true)
					TrueCounter++;
				boolean kair = k.isSelected();
				System.out.println(kair);
				if (kair == true)
					TrueCounter++;
				boolean lair = l.isSelected();
				System.out.println(lair);
				if (lair == true)
					TrueCounter++;
				if (TrueCounter > 1)
				JOptionPane.showMessageDialog(frame, "You have seletcted "+TrueCounter+" different Airlines. Do you want to continue?");
				else
				JOptionPane.showMessageDialog(frame, "You have selected only 1 Airline. Do you want to continue?"); 
				try {
					PrintWriter writer = new PrintWriter("AirlineLog.txt", "UTF-8");
					writer.println(aair);
					writer.println(bair);
					writer.println(cair);
					writer.println(dair);
					writer.println(mair);
					writer.println(fair);
					writer.println(gair);
					writer.println(hair);
					writer.println(iair);
					writer.println(jair);
					writer.println(kair);
					writer.println(lair);
					writer.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				//System.exit(0);
                Duration.main(null); 
                //Airline.main(null); 
			}
		});
		button.setBounds(407, 483, 165, 23);
		frame.getContentPane().add(button);
}
}
