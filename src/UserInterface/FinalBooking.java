package UserInterface;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;

public class FinalBooking {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FinalBooking window = new FinalBooking();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FinalBooking() {
		initialize();
	}

	String adults,babies,children,fromd,tod,froml,tol;
	private void initialize() {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(
					"PassangerLog.txt"));
			adults = reader.readLine();
			children = reader.readLine();
			babies = reader.readLine();
			System.out.println(adults+" "+children+" "+babies);
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			reader = new BufferedReader(new FileReader(
					"DurationLog.txt"));
			fromd = reader.readLine();
			tod = reader.readLine();
			//babies = reader.readLine();
			//System.out.println(adults+" "+children+" "+babies);
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			reader = new BufferedReader(new FileReader(
					"LocationLog.txt"));
			froml = reader.readLine();
			tol = reader.readLine();
			//babies = reader.readLine();
			//System.out.println(adults+" "+children+" "+babies);
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(218,68,83));
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("Airline Reservation System");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Product Sans", Font.PLAIN, 30));
		label.setBackground(Color.WHITE);
		label.setBounds(28, 22, 357, 37);
		frame.getContentPane().add(label);
		
		JLabel lblFinalBooking = new JLabel("Final Booking");
		lblFinalBooking.setForeground(Color.WHITE);
		lblFinalBooking.setFont(new Font("Product Sans", Font.PLAIN, 20));
		lblFinalBooking.setBackground(Color.WHITE);
		lblFinalBooking.setBounds(28, 56, 124, 37);
		frame.getContentPane().add(lblFinalBooking);
		
		int sum = Integer.parseInt(adults);	
		sum += Integer.parseInt(children);
		sum += Integer.parseInt(babies);
		
		JLabel lblTotalTickets = new JLabel("Total Tickets: "+sum);
		lblTotalTickets.setForeground(Color.WHITE);
		lblTotalTickets.setFont(new Font("Product Sans", Font.PLAIN, 22));
		lblTotalTickets.setBackground(Color.WHITE);
		lblTotalTickets.setBounds(28, 137, 255, 37);
		frame.getContentPane().add(lblTotalTickets);
		
		JLabel lblAdults = new JLabel("Adults: "+adults);
		lblAdults.setForeground(Color.WHITE);
		lblAdults.setFont(new Font("Product Sans", Font.PLAIN, 18));
		lblAdults.setBackground(Color.WHITE);
		lblAdults.setBounds(28, 176, 199, 37);
		frame.getContentPane().add(lblAdults);
		
		JLabel lblChildren = new JLabel("Children: "+children);
		lblChildren.setForeground(Color.WHITE);
		lblChildren.setFont(new Font("Product Sans", Font.PLAIN, 18));
		lblChildren.setBackground(Color.WHITE);
		lblChildren.setBounds(28, 224, 228, 37);
		frame.getContentPane().add(lblChildren);
		
		JLabel lblBabies = new JLabel("Babies: "+babies);
		lblBabies.setForeground(Color.WHITE);
		lblBabies.setFont(new Font("Product Sans", Font.PLAIN, 18));
		lblBabies.setBackground(Color.WHITE);
		lblBabies.setBounds(28, 272, 217, 37);
		frame.getContentPane().add(lblBabies);
		
		JLabel lblLocation = new JLabel("Location");
		lblLocation.setForeground(Color.WHITE);
		lblLocation.setFont(new Font("Product Sans", Font.PLAIN, 22));
		lblLocation.setBackground(Color.WHITE);
		lblLocation.setBounds(314, 137, 150, 37);
		frame.getContentPane().add(lblLocation);
		
		JLabel lblFrom_1 = new JLabel("From: "+froml);
		lblFrom_1.setForeground(Color.WHITE);
		lblFrom_1.setFont(new Font("Product Sans", Font.PLAIN, 18));
		lblFrom_1.setBackground(Color.WHITE);
		lblFrom_1.setBounds(314, 176, 181, 37);
		frame.getContentPane().add(lblFrom_1);
		
		JLabel lblTo_1 = new JLabel("To: "+tol);
		lblTo_1.setForeground(Color.WHITE);
		lblTo_1.setFont(new Font("Product Sans", Font.PLAIN, 18));
		lblTo_1.setBackground(Color.WHITE);
		lblTo_1.setBounds(314, 224, 181, 37);
		frame.getContentPane().add(lblTo_1);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setForeground(Color.WHITE);
		lblDate.setFont(new Font("Product Sans", Font.PLAIN, 22));
		lblDate.setBackground(Color.WHITE);
		lblDate.setBounds(314, 281, 181, 37);
		frame.getContentPane().add(lblDate);
		
		JLabel label_1 = new JLabel("To: "+tod);
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("Product Sans", Font.PLAIN, 18));
		label_1.setBackground(Color.WHITE);
		label_1.setBounds(314, 382, 199, 37);
		frame.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("From: "+ fromd);
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Product Sans", Font.PLAIN, 18));
		label_2.setBackground(Color.WHITE);
		label_2.setBounds(314, 334, 181, 37);
		frame.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("");
		label_3.setIcon(new ImageIcon(FinalBooking.class.getResource("/Images/Untitled design.png")));
		label_3.setForeground(Color.LIGHT_GRAY);
		label_3.setBackground(Color.LIGHT_GRAY);
		label_3.setBounds(600, 35, 284, 88);
		frame.getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("");
		label_4.setIcon(new ImageIcon(FinalBooking.class.getResource("/Images/Untitled design (1).png")));
		label_4.setForeground(Color.LIGHT_GRAY);
		label_4.setBackground(Color.LIGHT_GRAY);
		label_4.setBounds(600, 152, 284, 88);
		frame.getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("");
		label_5.setIcon(new ImageIcon(FinalBooking.class.getResource("/Images/Untitled design (2).png")));
		label_5.setForeground(Color.LIGHT_GRAY);
		label_5.setBackground(Color.LIGHT_GRAY);
		label_5.setBounds(600, 263, 284, 88);
		frame.getContentPane().add(label_5);
		
		JLabel label_6 = new JLabel("");
		label_6.setIcon(new ImageIcon(FinalBooking.class.getResource("/Images/Untitled design (3).png")));
		label_6.setForeground(Color.LIGHT_GRAY);
		label_6.setBackground(Color.LIGHT_GRAY);
		label_6.setBounds(600, 387, 284, 88);
		frame.getContentPane().add(label_6);
		
		JLabel label_7 = new JLabel("");
		label_7.setIcon(new ImageIcon(FinalBooking.class.getResource("/Images/Untitled design (7).png")));
		label_7.setForeground(Color.LIGHT_GRAY);
		label_7.setBackground(Color.LIGHT_GRAY);
		label_7.setBounds(968, 387, 284, 88);
		frame.getContentPane().add(label_7);
		
		JLabel label_8 = new JLabel("");
		label_8.setIcon(new ImageIcon(FinalBooking.class.getResource("/Images/Untitled design (6).png")));
		label_8.setForeground(Color.LIGHT_GRAY);
		label_8.setBackground(Color.LIGHT_GRAY);
		label_8.setBounds(968, 272, 284, 88);
		frame.getContentPane().add(label_8);
		
		JLabel label_9 = new JLabel("");
		label_9.setIcon(new ImageIcon(FinalBooking.class.getResource("/Images/Untitled design (5).png")));
		label_9.setForeground(Color.LIGHT_GRAY);
		label_9.setBackground(Color.LIGHT_GRAY);
		label_9.setBounds(968, 152, 284, 88);
		frame.getContentPane().add(label_9);
		
		JLabel label_10 = new JLabel("");
		label_10.setIcon(new ImageIcon(FinalBooking.class.getResource("/Images/Untitled design (4).png")));
		label_10.setForeground(Color.LIGHT_GRAY);
		label_10.setBackground(Color.LIGHT_GRAY);
		label_10.setBounds(968, 40, 284, 88);
		frame.getContentPane().add(label_10);
		
		JLabel label_11 = new JLabel("");
		label_11.setIcon(new ImageIcon(FinalBooking.class.getResource("/Images/Untitled design (8).png")));
		label_11.setForeground(Color.LIGHT_GRAY);
		label_11.setBackground(Color.LIGHT_GRAY);
		label_11.setBounds(1293, 40, 284, 88);
		frame.getContentPane().add(label_11);
		
		JLabel label_12 = new JLabel("");
		label_12.setIcon(new ImageIcon(FinalBooking.class.getResource("/Images/Untitled design (9).png")));
		label_12.setForeground(Color.LIGHT_GRAY);
		label_12.setBackground(Color.LIGHT_GRAY);
		label_12.setBounds(1293, 152, 284, 88);
		frame.getContentPane().add(label_12);
		
		JLabel label_13 = new JLabel("");
		label_13.setIcon(new ImageIcon(FinalBooking.class.getResource("/Images/Untitled design (10).png")));
		label_13.setForeground(Color.LIGHT_GRAY);
		label_13.setBackground(Color.LIGHT_GRAY);
		label_13.setBounds(1293, 263, 284, 88);
		frame.getContentPane().add(label_13);
		
		JLabel label_14 = new JLabel("");
		label_14.setIcon(new ImageIcon(FinalBooking.class.getResource("/Images/Untitled design (11).png")));
		label_14.setForeground(Color.LIGHT_GRAY);
		label_14.setBackground(Color.LIGHT_GRAY);
		label_14.setBounds(1293, 387, 284, 88);
		frame.getContentPane().add(label_14);
		
		JCheckBox a = new JCheckBox("");
		a.setBackground(new Color(218,68,83));
		a.setBounds(562, 66, 35, 23);
		frame.getContentPane().add(a);
		
		JCheckBox b = new JCheckBox("");
		b.setBackground(new Color(218, 68, 83));
		b.setBounds(562, 186, 35, 23);
		frame.getContentPane().add(b);
		
		JCheckBox c = new JCheckBox("");
		c.setBackground(new Color(218, 68, 83));
		c.setBounds(562, 297, 35, 23);
		frame.getContentPane().add(c);
		
		JCheckBox d = new JCheckBox("");
		d.setBackground(new Color(218, 68, 83));
		d.setBounds(562, 426, 27, 23);
		frame.getContentPane().add(d);
		
		JCheckBox m = new JCheckBox("");
		m.setBackground(new Color(218, 68, 83));
		m.setBounds(938, 66, 27, 23);
		frame.getContentPane().add(m);
		
		JCheckBox f = new JCheckBox("");
		f.setBackground(new Color(218, 68, 83));
		f.setBounds(938, 186, 21, 23);
		frame.getContentPane().add(f);
		
		JCheckBox g = new JCheckBox("");
		g.setBackground(new Color(218, 68, 83));
		g.setBounds(938, 297, 24, 23);
		frame.getContentPane().add(g);
		
		JCheckBox h = new JCheckBox("");
		h.setBackground(new Color(218, 68, 83));
		h.setBounds(938, 426, 24, 23);
		frame.getContentPane().add(h);
		
		JCheckBox i = new JCheckBox("");
		i.setBackground(new Color(218, 68, 83));
		i.setBounds(1258, 66, 21, 23);
		frame.getContentPane().add(i);
		
		JCheckBox j = new JCheckBox("");
		j.setBackground(new Color(218, 68, 83));
		j.setBounds(1258, 190, 21, 23);
		frame.getContentPane().add(j);
		
		JCheckBox k = new JCheckBox("");
		k.setBackground(new Color(218, 68, 83));
		k.setBounds(1258, 300, 21, 23);
		frame.getContentPane().add(k);
		
		JCheckBox l = new JCheckBox("");
		l.setBackground(new Color(218, 68, 83));
		l.setBounds(1258, 411, 21, 23);
		frame.getContentPane().add(l);
		
		JLabel lblAllAirlinesAre = new JLabel("All Airlines are available.");
		lblAllAirlinesAre.setForeground(Color.WHITE);
		lblAllAirlinesAre.setFont(new Font("Product Sans", Font.PLAIN, 22));
		lblAllAirlinesAre.setBackground(Color.WHITE);
		lblAllAirlinesAre.setBounds(130, 539, 255, 37);
		frame.getContentPane().add(lblAllAirlinesAre);
		
		JButton btnBookYourTrip = new JButton("Book your trip");
		btnBookYourTrip.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int TrueCounter = 0;
				boolean aair = a.isSelected();
				System.out.println(aair);
				if (aair == true)
					TrueCounter++;
				boolean bair = b.isSelected();
				System.out.println(bair);
				if (bair == true)
					TrueCounter++;
				boolean cair = c.isSelected();
				System.out.println(cair);
				if (cair == true)
					TrueCounter++;
				boolean dair = d.isSelected();
				System.out.println(dair);
				if (dair == true)
					TrueCounter++;
				boolean mair = m.isSelected();
				System.out.println(mair);
				if (mair == true)
					TrueCounter++;
				boolean fair = f.isSelected();
				System.out.println(fair);
				if (fair == true)
					TrueCounter++;
				boolean gair = g.isSelected();
				System.out.println(gair);
				if (gair == true)
					TrueCounter++;
				boolean hair = h.isSelected();
				System.out.println(hair);
				if (hair == true)
					TrueCounter++;
				boolean iair = i.isSelected();
				System.out.println(iair);
				if (iair == true)
					TrueCounter++;
				boolean jair = j.isSelected();
				System.out.println(jair);
				if (jair == true)
					TrueCounter++;
				boolean kair = k.isSelected();
				System.out.println(kair);
				if (kair == true)
					TrueCounter++;
				boolean lair = l.isSelected();
				System.out.println(lair);
				if (lair == true)
					TrueCounter++;
				if (TrueCounter > 1) {
				JOptionPane.showMessageDialog(frame, "You should only select one Airline! Please try again!");
				Unsuccessful.main(null);
				}
				else if (TrueCounter == 0) {
					JOptionPane.showMessageDialog(frame, "You should anyone Airline! Please try again!");
					Unsuccessful.main(null); 
				}
				else
				JOptionPane.showMessageDialog(frame, "Are you sure with this Airline?. Do you want to continue?"); 
				try {
					PrintWriter writer = new PrintWriter("AirlineLog.txt", "UTF-8");
					writer.println(aair);
					writer.println(bair);
					writer.println(cair);
					writer.println(dair);
					writer.println(mair);
					writer.println(fair);
					writer.println(gair);
					writer.println(hair);
					writer.println(iair);
					writer.println(jair);
					writer.println(kair);
					writer.println(lair);
					writer.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				//System.exit(0);
                EnjoyTrip.main(null); 
                //Airline.main(null); 
			}
		});
		btnBookYourTrip.setFont(new Font("Product Sans", Font.PLAIN, 15));
		btnBookYourTrip.setBounds(1421, 548, 134, 23);
		frame.getContentPane().add(btnBookYourTrip);
		frame.setBounds(100, 100, 1615, 626);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//int TrueCounter;
		
	}
}
	
